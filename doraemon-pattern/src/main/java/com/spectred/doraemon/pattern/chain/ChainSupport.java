package com.spectred.doraemon.pattern.chain;

import lombok.Data;

/**
 * 责任链运行至某一阶段时是否继续
 * FIXME  多种不同的责任链可能会互相影响，考虑给链加标识
 */
@Data
public class ChainSupport {


    private static final ThreadLocal<ChainSupport> SUPPORT_THREAD_LOCAL = ThreadLocal.withInitial(ChainSupport::new);

    /**
     * 责任链是否继续的状态
     */
    private boolean shouldTransmit;

    private ChainSupport() {
        // 初始赋值
        this.shouldTransmit = true;
    }

    /**
     * 是否应该继续链的向下传递
     */
    public static boolean shouldTransmit() {
        return SUPPORT_THREAD_LOCAL.get().isShouldTransmit();
    }

    /**
     * 打断链,不再向下传递
     */
    public static void brokeChain() {
        SUPPORT_THREAD_LOCAL.get().setShouldTransmit(false);
    }

    public static void remove() {
        SUPPORT_THREAD_LOCAL.remove();
    }

}
