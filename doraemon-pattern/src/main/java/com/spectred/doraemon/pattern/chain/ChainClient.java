package com.spectred.doraemon.pattern.chain;

import com.spectred.doraemon.pattern.chain.concrete.ConcreteChainHandlerA;
import com.spectred.doraemon.pattern.chain.concrete.ConcreteChainHandlerB;
import com.spectred.doraemon.pattern.chain.concrete.ConcreteChainHandlerC;

/**
 * @author SWD
 */
public class ChainClient {
    public static void main(String[] args) {
        new Thread(() -> {
            Chain chain = new Chain();
            chain.next(new ConcreteChainHandlerA())
                    .next(new ConcreteChainHandlerB())
                    .next(new ConcreteChainHandlerC());
            String msg = "A";
            String str = chain.handle(msg);
            System.out.println(Thread.currentThread().getName() + ":" + str);
            ChainSupport.remove();
        }, "A").start();

        new Thread(() -> {
            Chain chain = new Chain();
            chain.next(new ConcreteChainHandlerA())
                    .next(new ConcreteChainHandlerB())
                    .next(new ConcreteChainHandlerC());
            String msg = "A";
            String str = chain.handle(msg);
            System.out.println(Thread.currentThread().getName() + ":" + str);
            ChainSupport.remove();
        }, "B").start();

        new Thread(() -> {
            Chain chain = new Chain();
            chain.next(new ConcreteChainHandlerA())
                    .next(new ConcreteChainHandlerB())
                    .next(new ConcreteChainHandlerC());
            String msg = "A";
            String str = chain.handle(msg);
            System.out.println(Thread.currentThread().getName() + ":" + str);
            ChainSupport.remove();
        }, "C").start();
    }
}
