package com.spectred.doraemon.pattern.chain;

import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicReference;

public class Chain {
    // 触发CI
    private List<ChainHandler> chains = new CopyOnWriteArrayList<>();

    public Chain next(ChainHandler handler) {
        this.chains.add(handler);
        return this;
    }

    public String handle(String msg) {
        for (ChainHandler chain : chains) {
            if (ChainSupport.shouldTransmit()) {
                msg = chain.handle(msg);
            }
        }
        return msg;
    }
}
