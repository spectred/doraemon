package com.spectred.doraemon.pattern.chain.concrete;

import com.spectred.doraemon.pattern.chain.ChainHandler;

public class ConcreteChainHandlerA implements ChainHandler {

    @Override
    public String handle(String msg) {
        return msg+"-A";
    }
}
