package com.spectred.doraemon.pattern.chain.concrete;

import com.spectred.doraemon.pattern.chain.ChainHandler;
import com.spectred.doraemon.pattern.chain.ChainSupport;

public class ConcreteChainHandlerB implements ChainHandler {
    @Override
    public String handle(String msg) {
        // 线程C将打断此链，不再继续执行
        if ("C".equals(Thread.currentThread().getName())) {
            ChainSupport.brokeChain();
        }
        return msg + "-B";
    }
}
