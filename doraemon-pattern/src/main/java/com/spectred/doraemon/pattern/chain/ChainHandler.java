package com.spectred.doraemon.pattern.chain;

public interface ChainHandler {

    String handle(String msg);
}
