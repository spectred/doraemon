package com.spectred.doraemon.mq.rocketmq.consumer;

import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyContext;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.common.message.MessageExt;

import java.util.List;

public class RocketMQReceiveTest {

    private static final String NAME_SRV_ADDR = "127.0.0.1:9876";

    public static void main(String[] args) throws MQClientException {
        // 1. 创建消息消费者，指定消费者所属的组名
        DefaultMQPushConsumer consumer = new DefaultMQPushConsumer("demo-group");
        // 2. 设NameSrv地址
        consumer.setNamesrvAddr(NAME_SRV_ADDR);
        // 3. 指定消费者订阅的主题和标签
        consumer.subscribe("demo-topic", "*");
        // 4. 设置回调函数，编写处理消息的方法
        consumer.registerMessageListener(new MessageListenerConcurrently() {
            @Override
            public ConsumeConcurrentlyStatus consumeMessage(List<MessageExt> list, ConsumeConcurrentlyContext consumeConcurrentlyContext) {
                System.out.println("消费者-接收到消息:" + list);
                return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
            }
        });
        // 5. 启动消息消费者
        consumer.start();

        System.out.println("Consumer Started.");
    }
}
