package com.spectred.doraemon.mq.rocketmq.producer;

import org.apache.rocketmq.client.exception.MQBrokerException;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.SendResult;
import org.apache.rocketmq.common.message.Message;
import org.apache.rocketmq.remoting.exception.RemotingException;

public class RocketMQSendMsgTest {

    private static final String NAME_SRV_ADDR = "127.0.0.1:9876";

    public static void main(String[] args) throws MQClientException, RemotingException, InterruptedException, MQBrokerException {
        // 1. 创建消息生产者，并设置生产组名
        DefaultMQProducer producer = new DefaultMQProducer("demo-group");
        // 2. 为生产者设置NameSrv地址
        producer.setNamesrvAddr(NAME_SRV_ADDR);
        // 3. 启动生产者
        producer.start();
        // 4. 构建消息对象，主要是设置消息的主题 标签 内容
        Message message = new Message("demo-topic", "demo-tag", "Hello RocketMQ!".getBytes());
        // 5. 发送消息
        SendResult result = producer.send(message);
        System.out.println(result);
        // 6. 关闭生产者
        producer.shutdown();
    }
}
